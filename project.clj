(defproject djdash "0.1.50"
  :description "Dashboard for SPAZ Radio"
  :url "http://spaz.org/radio"

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.339"] 
                 [cljs-http "0.1.45"]
                 [com.taoensso/sente "1.13.0" :exclusions [io.aviso/pretty com.taoensso/encore]]
                 [prismatic/schema "1.1.9"]
                 [hiccup "1.0.5"]
                 [com.stuartsierra/component "0.3.2"]
                 [clojurewerkz/machine_head "1.0.0"
                  :exclusions [com.google.guava/guava]]
                 [org.clojure/data.zip "0.1.2"]
                 [camel-snake-kebab "0.4.0"]
                 [ring.middleware.jsonp "0.1.6" 
                  :exclusions [ring/ring-core]]
                 [robert/bruce "0.8.0"]
                 [reagent "0.8.1"
                  :exclusions [org.clojure/tools.reader]]
                 [reagent-forms "0.5.42"]
                 [reagent-utils "0.3.1"]
                 [javax.xml.bind/jaxb-api "2.3.0"]
                 [compojure "1.6.1"] 
                 [ring "1.6.3"]
                 [incanter/incanter-charts "1.9.3"
                  :exclusions [junit]]
                 [incanter/incanter-pdf "1.9.3"]
                 [hikari-cp "2.6.0"]
                 [org.clojure/java.jdbc "0.7.7"]
                 [org.postgresql/postgresql "42.2.4"]
                 [migratus "1.0.8"
                  :exclusions [org.clojure/clojure]]
                 [honeysql "0.9.3"]
                 [environ "1.1.0"]
                 [com.taoensso.forks/http-kit "2.1.20"]
                 [utilza "0.1.98"]
                 [org.clojure/tools.nrepl "0.2.13"]
                 [clj-ical "1.1" :exclusions [clj-time]]
                 [org.clojure/data.xml "0.0.8"]
                 [clj-time "0.14.4"]
                 [com.taoensso/timbre "4.10.0"]
                 [org.clojure/tools.reader "1.3.0"]
                 [cheshire "5.8.0"]
                 [stencil "0.5.0"
                  :exclusions [org.clojure/core.cache]]
                 [clj-http "3.9.1"]
                 [com.andrewmcveigh/cljs-time "0.5.2" :exclusions [com.cemerick/austin]]
                 [org.clojure/tools.trace "0.7.9"]
                 [org.clojure/core.async "0.4.474"]
                 ]
  
  ;;  :hooks [leiningen.cljsbuild] ;; suppoed to enable cljs testing, but doesn't.test
  :main djdash.core

  ;; XXX HACK, cough, HACK
  :migratus ~(let [{:keys [host db port user password]} (try (-> "config.edn"
                                                                 slurp
                                                                 read-string
                                                                 :db)
                                                             (catch Exception e
                                                               (println "OK no config.edn, no migrations"))) ]
               {:store :database
                :migration-dir "migrations/"
                :db {:classname "com.postgresql.Driver"
                     :subprotocol "postgresql"
                     :subname (str "//" host ":" port "/" db)
                     :user user
                     :password password}})
  :source-paths ["clj-src" "cljs-src"]
  :clean-targets ^{:protect false} [[:cljsbuild :builds "dev" :compiler :output-dir] 
                                    [:cljsbuild :builds "dev" :compiler :output-to]
                                    [:cljsbuild :builds "release" :compiler :output-dir] 
                                    [:cljsbuild :builds "release" :compiler :output-to]
                                    :target]
  :profiles {:dev {:repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                   :jvm-opts ["-XX:-OmitStackTraceInFastThrow"]
                   :source-paths ["clj-src" "cljs-src" "user"]
                   :plugins [[lein-cljsbuild "1.1.7"]
                             [lein-ancient "0.6.15"]
                             [lein-pdo "0.1.1"]
                             [lein-difftest "2.0.0"]
                             [lein-figwheel "0.5.16"
                              :exclusions [org.clojure/tools.reader
                                           org.clojure/clojure
                                           ring/ring-core]]
                             [migratus-lein "0.5.9"
                              :exclusions [org.clojure/clojure]]] 
                   :dependencies [[org.slf4j/log4j-over-slf4j "1.7.25"]
                                  [org.slf4j/slf4j-simple "1.7.25"]
                                  ;; for migratus. can't use timbre-slf4j because
                                  ;; http://yogthos.net/posts/2015-12-26-AOTGotchas.html
                                  [com.cemerick/piggieback "0.2.2"]
                                  [weasel "0.7.0" :exclusions [org.clojure/clojurescript]]
                                  [figwheel-sidecar "0.5.16"
                                   :exclusions [org.clojure/tools.reader
                                                org.clojure/core.async
                                                http-kit
                                                ring/ring-core]]]}
             :test {:source-paths ["clj-src" "cljs-src"]
                    ;; :cloverage {}
                    :plugins [[lein-cloverage "1.0.11"]]}
             :uberjar {:prep-tasks [["cljsbuild" "once" "release"] "javac" "compile"]
                       :uberjar-name "djdash.jar"
                       :source-paths ["clj-src" "cljs-src"]
                       :aot :all}
             :release {:jvm-opts ["-XX:-OmitStackTraceInFastThrow"
                                  "-Xms32m"
                                  "-Xmx512m"
                                  "-XX:MaxMetaspaceSize=56m"]}
             :repl {:timeout 180000
                    :injections [(do ;; implicit?
                                   (require 'user)
                                   (user/start) ;; for figwheel
                                   (require 'djdash.core)
                                   (djdash.core/-main))
                                 ]}}

  :cljsbuild {:builds {"dev" {:source-paths ["cljs-src" "repl"]
                              :figwheel true
                              :compiler {:output-to "resources/public/js/djdash.js"
                                         :output-dir "resources/public/js/dev"
                                         :asset-path "js/dev"
                                         :main djdash.core
                                         :optimizations :none
                                         :source-map  true}}
                       "release"{:source-paths ["cljs-src" "mock-fake-cljs"]
                                 :jar true
                                 :compiler {:output-dir "resources/public/js/release"
                                            :output-to "resources/public/js/djdash-min.js"
                                            ;;:output-wrapper true ;; don't know why this would be necessary?
                                            :optimizations :advanced
                                            :pretty-print false
                                            :closure-warnings {:externs-validation :off
                                                               :non-standard-jsdoc :off}
                                            :source-map  "resources/public/js/djdash.js.map"
                                            :externs ["resources/public/js/jquery.min.js"
                                                      "resources/public/js/jquery.flot.time.min.js"
                                                      "resources/public/js/jquery.flot.min.js"]}}}}
  :aliases {"tr" ["with-profile" "+user,+dev,+server"
                  "pdo" "cljsbuild" "once" "dev," "trampoline" "repl" ":headless"]
            "slamhound" ["run" "-m" "slam.hound" "clj-src/"]
            "devbuild" ["cljsbuild" "auto" "dev"]})


