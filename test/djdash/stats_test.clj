(ns djdash.stats-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [djdash.stats :as stats :refer :all]
            [schema.core :as s]))


(deftest parse-clients-test
  (is    (= (->> "test-data/clients.xml"
                 slurp
                 parse-clients)
            (->> "test-data/clients.edn"
                 slurp
                 edn/read-string))))


(deftest parse-mounts-test
  (is (= (->> "test-data/stats.xml"
              slurp
              parse-mounts)
         '("/radio-low.ogg" "/radio.ogg"))))

(deftest combine-stats-test
  (is (= (->> "test-data/clients.edn"
              slurp
              edn/read-string
              vector
              combine-stats)
         (->> "test-data/combined.edn"
              slurp
              edn/read-string))))


(deftest total-listener-count-test
  (is (= (->> "test-data/combined.edn"
              slurp
              edn/read-string
              total-listener-count)
         3)))

(deftest listener-details-test
  (is (= (listener-details '{"/radio" {:count 2,
                                       :listeners ({:ip "fakaye",
                                                    :user-agent "Mozilla/5.0 (iPhone; CPU iPhone OS 15_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/103.0.5060.63 Mobile/15E148 Safari/604.1",
                                                    :connected 33,
                                                    :id 2039251}
                                                   {:ip "feekey",
                                                    :user-agent "Mozilla/5.0 (iPhone; CPU iPhone OS 15_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/103.0.5060.63 Mobile/15E148 Safari/604.1",
                                                    :connected 31,
                                                    :id 2039252})}})
         '({:ip "fakaye",
            :user-agent "Mozilla/5.0 (iPhone; CPU iPhone OS 15_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/103.0.5060.63 Mobile/15E148 Safari/604.1",
            :id 2039251}
           {:ip "feekey", :user-agent "Mozilla/5.0 (iPhone; CPU iPhone OS 15_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/103.0.5060.63 Mobile/15E148 Safari/604.1",
            :id 2039252}))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (ulog/catcher
   (run-tests))
  
  (ulog/spewer


   
   
   )
  
  
  )

