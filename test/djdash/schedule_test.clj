(ns djdash.schedule-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [utilza.repl :as urepl]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [djdash.schedule :as schedule :refer :all]
            [schema.core :as s]))


(deftest fetch-test
  (is (= (fetch-schedule "test-data/week-info.json")
         (-> "test-data/week-info.edn"
             slurp
             edn/read-string))))


(deftest split-by-current-test
  (is (= (->> "test-data/week-info.edn"
              slurp
              edn/read-string
              (split-by-current #inst "2018-08-28T20:00:00.000-00:00") )
         (-> "test-data/current-split-1.edn"
             slurp
             edn/read-string))))

(deftest up-next-test
  (let [{:keys [future current]} (-> "test-data/current-split-1.edn"
                                     slurp
                                     edn/read-string)]
    (is (= (->up-next current future)
           (slurp "test-data/up-next.json")))  ))


(deftest jsonify-date-test
  (is (= (jsonify-date #inst "2018-08-10T02:58:46.145-00:00")
         1533869926145)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  
  (ulog/catcher
   (run-tests))
  

  )


(comment
  
  (ulog/spewer
   (fetch-schedule "test-data/week-info.json"))

  (urepl/massive-spew "test-data/current-split-1.edn"  *1)

  (ulog/catcher
   (let [{:keys [future current]} (-> "test-data/current-split-1.edn"
                                      slurp
                                      edn/read-string)]
     (->up-next current future)))

  (spit "test-data/up-next.json"  *1)

  )

(comment

;;; TODO;
  (require '[clojure.edn :as edn])

  (let [d (java.util.Date.)
        {:keys [url]} (->> @sys/system :scheduler :settings)
        {:keys [current future] :as old}  (->> "test-data/broken-schedule.edn" 
                                               slurp 
                                               edn/read-string)]
    (let [{:keys [current future] :as new-sched} (->> (concat current future) ;; rejoining for resplitting
                                                      (split-by-current d))]
      (-> (or (some->> url fetch-schedule  (split-by-current d))
              new-sched)
          ;; don't need to keep all the old currents!
          (update-in [:current] #(-> % last vector)))))

  )

