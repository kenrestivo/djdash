(ns djdash.web.shows-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [clj-time.coerce :as coerce]
            [clojure.data :as data]
            [utilza.repl :as urepl]
            [clojure.string :as str]
            [djdash.web.shows :refer :all]
            [schema.core :as s]))


(deftest cal-to-html-test
  (is (= (->> "test-data/calendar.edn"
              slurp
              edn/read-string
              cal->html)
         (->> "test-data/calendar.html"
              slurp))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (ulog/catcher
   (run-tests))

  (ulog/catcher
   )
  



  )


