(ns djdash.schedule.public-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [utilza.misc :as umisc]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [clj-time.coerce :as coerce]
            [clojure.data :as data]
            [utilza.repl :as urepl]
            [clojure.string :as str]
            [djdash.schedule.public :refer :all]
            [schema.core :as s]))


(def fancy-readers (merge default-data-readers
                          *data-readers*
                          ;; hack to turn the jodatime objects into strings
                          {'object #(some-> % last str)}))


(deftest tz-shift-test
  (let [offset -4]
    (is (= (-> offset
               (tz-shift  "2018-08-11T12:34:00.000-08:00")
               str)
           "2018-08-11T16:34:00.000-04:00")) ))

(deftest date-adjust-test
  (let [offset -2]
    (is (= (->> "test-data/current-split-1.edn"
                slurp
                edn/read-string
                :future
                (map #(umisc/munge-columns (date-adjust offset) %))
                pr-str ;; neded to handle serializing dates
                (edn/read-string  {:readers fancy-readers}))
           (->> "test-data/schedule-date-adjust.edn"
                slurp
                edn/read-string)))))


(deftest insert-actual-test
  (is (= (->> "test-data/schedule-date-adjust.edn"
              slurp
              edn/read-string
              (map insert-actual))
         (->> "test-data/schedule-insert-actual.edn"
              slurp
              edn/read-string))))

;; XXX this is failing, for no obvious reason.
;; perhaps the test is dependent on datetime it is run? i hope not...
#_(deftest calendar-test
    (is    (= (->> "test-data/current-split-1.edn"
                   slurp
                   edn/read-string
                   (calendar -8)
                   ;; this nonsense round-trip is necessary because jodatime
                   pr-str
                   (edn/read-string  {:readers fancy-readers}))
              (->> "test-data/calendar.edn"
                   slurp
                   edn/read-string))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (ulog/catcher
   (run-tests))


  (ulog/spewer
   (let [offset -2]
     (->> "test-data/current-split-1.edn"
          slurp
          edn/read-string
          :future
          (map #(umisc/munge-columns (date-adjust offset) %))
          pr-str ;; neded to handle serializing dates
          (edn/read-string  {:readers fancy-readers}))))


  (urepl/massive-spew "test-data/schedule-insert-actual.edn" *1)
  
  ;; XXX not working yet
  (ulog/spewer
   (let [offset -8
         relative-now (tz-shift offset "2018-08-11T20:00:00.000-08:00")]
     (->> "test-data/schedule-insert-actual.edn"
          slurp
          edn/read-string
          ;; TODO: have to bring the date string back from the dead, and turn it into datetime again, b4 testing
          (one-week-only relative-now))))


  
  (ulog/catcher
   )

  )
