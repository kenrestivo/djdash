(ns djdash.schedule.ical-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [djdash.schedule.ical :as ical :refer :all]
            [schema.core :as s]))



(deftest ical-test
  (is (= (->> "test-data/current-split-1.edn"
              slurp
              edn/read-string
              ->ical)
         (->> "test-data/sched.ical"
              slurp))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  (ulog/catcher
   (run-tests))
  
  (ulog/catcher
   )

  (spit "test-data/sched.ical" *1)
  
  )

