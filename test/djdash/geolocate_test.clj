(ns djdash.geolocate-test
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [taoensso.timbre :as log]
            [clojure.edn :as edn]
            [utilza.log :as ulog]
            [djdash.geolocate :as geolocate :refer :all]
            [schema.core :as s]))


(deftest munge-geo-test
  (is (= (munge-geo {:regionName "Uusimaa",
                     :zipCode "00100",
                     :timeZone "+03:00",
                     :statusMessage "",
                     :longitude "24.9354",
                     :countryName "Finland",
                     :cityName "Helsinki",
                     :latitude "60.1695",
                     :ipAddress "95.216.40.15",
                     :statusCode "OK",
                     :countryCode "FI"})
         {:city "Helsinki",
          :country "Finland",
          :lng 24.9354,
          :ip "95.216.40.15",
          :lat 60.1695,
          :region "Uusimaa"})))

(deftest merge-and-keyify-geo-test
  (is (= (merge-and-keyify-geo {:ip "fakeyfakey",
                                :user-agent "Mozilla/5.0 (iPhone; CPU iPhone OS 15_8 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) CriOS/103.0.5060.63 Mobile/15E148 Safari/604.1",
                                :connected 28,
                                :id 2039251}
                               {:city "Bisbee",
                                :country "United States Of America",
                                :lng -109.928,
                                :ip "fakeyfakey",
                                :lat 31.4482,
                                :region "Arizona"})
         {2039251 {:ip "fakeyfakey",
                   :city "Bisbee",
                   :country "United States Of America",
                   :lng -109.928,
                   :lat 31.4482,
                   :region "Arizona"}})))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  
  (ulog/catcher
   (run-tests))


  
  

  )


(comment
  
  (ulog/spewer
   ) 



  

  
  
  


  )




(comment




  )
