(ns djdash.chat.chatlog
  (:require [clojure.core.async :as async]
            [honeysql.helpers :as h]
            [djdash.utils :as utils]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [cheshire.core :as json]
            [utilza.misc :as umisc]
            [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]))

(def chat-log-limit 100) ;; TODO: put in conf file and thread it all the way down here 

;; force dates to be milliseconds since epoch in utc
(def mungings {:time_received (fn [d] (.getTime d))})

(defn get-from-db
  [conn]
  (try
    (->> {:select [[:username :user] :message :time_received]
          :from [:messages]
          :order-by [[:time_received :desc]]
          :limit chat-log-limit}
         sql/format
         (jdbc/query conn)
         (map #(umisc/munge-columns mungings %))
         reverse
         (assoc {:status "OK"} :history)) ;; weird gyration
    (catch Exception e
      (log/error (.getCause e)))))


(defn get-log
  [dbc]
  (try
    (-> dbc
        get-from-db
        json/encode)
    (catch Exception e
      (log/error e))))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment

  (do
    (require '[clj-http.client :as client])
    (require '[djdash.core :as sys])
    (require '[utilza.log :as ulog])
    (require '[utilza.repl :as urepl])
    )  



  (->> (client/get "http://localhost:8080/chatlog"
                   {:as :json})
       :body
       (urepl/massive-spew "/tmp/foo.edn"))


 (->> (client/get "http://localhost:8080/chatlog"
                  {:query-params {:callback "foobar"}})
;;       :body
       (urepl/massive-spew "/tmp/foo.edn"))


  (-> @sys/system :db :conn)

  (ulog/spewer
   (-> @sys/system :db :conn get-from-db))


  (urepl/hjall
   (-> (java.util.Date.)
       .toInstant
       .toEpochMilli))


  (urepl/hjall
   #inst "2021-01-21T08:03:11.185659000-00:00")



  (ulog/catcher
   (-> #inst "2021-01-21T08:03:11.185659000-00:00"
       .getTime))
  )
