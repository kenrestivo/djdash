(ns djdash.core
  (:require [clojure.tools.namespace.repl :as trepl]
            [com.stuartsierra.component :as component]
            [djdash.geolocate :as geo]
            [taoensso.timbre :as log]
            [djdash.log :as dlog]
            [utilza.log :as ulog]
            [djdash.nrepl :as n]
            [djdash.sente :as sente]
            [schema.core :as s]
            [djdash.conf :as conf]
            [djdash.db :as db]
            [djdash.mqtt :as mqtt]
            [djdash.chat :as chat]
            [djdash.matrix :as matrix]
            [djdash.nowplaying :as nowplaying]
            [djdash.schedule :as schedule]
            [djdash.server :as srv]
            [djdash.tail :as tail])
  (:gen-class))


(defonce system (atom nil))


(defn make-system
  [{:keys [timbre tailer web-server chat mqtt nrepl db geo scheduler matrix now-playing]}]
  ;; TODO: hack! just use schema
  {:pre  [(every? identity (map map? [timbre chat mqtt db tailer matrix scheduler 
                                      now-playing geo web-server]))]} 
  (component/system-map
   :log (dlog/start-log timbre)
   :tailer (tail/create-tailer tailer)
   :db   (db/create-db db)
   :chat (chat/create-chat chat)
   :mqtt (mqtt/create-mqtt mqtt)
   :nrepl (n/create-nrepl nrepl)
   :sente (sente/create-sente)
   :geo   (geo/create-geo geo)
   :matrix   (matrix/create-matrix matrix)
   :nowplaying (nowplaying/create-nowplaying now-playing)
   :scheduler (schedule/create-scheduler scheduler)
   :web-server (srv/start-server web-server)
   ))


(defn init
  ;; TODO: shouldn't this take params??
  [options]
  (reset! system (make-system options)))

(defn start
  []
  (swap! system component/start))


(defn stop
  []
  (swap! system #(when % (component/stop %) nil)))


(defn go
  [options]
  (init options)
  (start))

(defn reset []
  (stop)
  #_(trepl/refresh))


(defn -main
  [& [conf-file-arg & _]]
  (try
    (let [conf-file (or conf-file-arg "config.edn")
          conf (conf/read-and-validate conf-file)]
      (println "Starting dashboard components" conf-file conf)
      (go conf))
    (catch Exception e
      (println (.getMessage e))
      (println (.getCause e)))))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  (start)
  (stop)
  (reset)

  
  (log/error *e)

  (def foo *1)
  
  (future-done? foo)
  
  (into {} @system)
  


  (reload (assoc-in env/env [:web-server :mode] :release))

  (init (assoc-in env/env [:web-server :mode] :release))
  (start)
  
  (stop)
  



  




  
  )


