(ns djdash.log
  (:require [com.stuartsierra.component :as component]
            [taoensso.timbre :as log]
            [utilza.java :as ujava]
            [djdash.utils :as utils]
            [clojure.tools.trace :as trace]
            [taoensso.timbre.appenders.core :as appenders]))

(defn revision-info
  "Utility for determing the program's revision."
  []
  (let [{:keys [version revision]} (ujava/get-project-properties "djdash" "djdash")]
    (format "Version: %s, Revision %s" version revision)))

(defrecord Log [config altered]
  component/Lifecycle
  (start
    [this]
    (if altered
      this
      (let [{:keys [spit-filename tracer]} config]
        (println "starting logging")
        (log/merge-config! (cond-> (assoc config
                                          :output-fn
                                          (partial log/default-output-fn {:stacktrace-fonts {}}))
                             spit-filename (assoc :appenders {:println nil ;; no, do not.
                                                              :spit (appenders/spit-appender
                                                                     {:fname spit-filename})})))
        (when tracer
          (alter-var-root #'clojure.tools.trace/tracer (fn [_]
                                                         (fn [name value]
                                                           (log/debug name value)))))
        (log/info "Welcome to DJ Dashboard" (revision-info))
        (log/info "logging started" config)
        ;; XXX why :altered?
        (assoc this :altered true))))
  (stop [this]
    this))

(defn start-log
  [config]
  (map->Log {:config config}))


(comment

  (log/error (Exception. "foobar"))
  
  (clojure.tools.trace/trace-vars #'foobar)
  
  )
