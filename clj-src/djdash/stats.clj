(ns djdash.stats
  (:require [camel-snake-kebab.core :as convert]
            [clj-http.client :as client]
            [clojure.data.zip.xml :as zx]
            [djdash.utils :as utils]
            [clojure.java.io :as io]
            [clojure.xml :as xml]
            [clojure.zip :as zip]
            [taoensso.timbre :as log]
            [utilza.core :as utilza]
            [utilza.misc :as umisc]))


(def listener-map {:connected #(Long/parseLong %)
                   :id #(Long/parseLong %)})


;; used by now playing
(defn total-listener-count
  [m]
  (->> m
       vals
       (map :count)
       (apply +)))


;; used by now playing
(defn listener-details
  "Takes a map of listener info from icecast separated by mount point.
  Combines the listener details into one map and removes the connected time."
  [m]
  (some->> m
           vals
           (map :listeners)
           (mapcat identity)
           (map #(dissoc % :connected))))


(defn parse-xml
  "Takes string, returns zipped xml"
  [s]
  (try
    (-> s
        (.getBytes "utf-8")
        io/input-stream
        xml/parse
        zip/xml-zip)
    (catch Exception e
      (log/error e s))))


(defn xml-nodes-to-map
  "Convert a seq of annoying XML nodes to a proper clojure map"
  [ns]
  (->> (for [n ns]
         [(-> n :tag convert/->kebab-case) 
          (-> n
              :content
              first)])
       (into {})
       (umisc/munge-columns listener-map)))


(defn listeners
  "Oh gawd what a nightmare"
  [n]
  (->> n
       :content
       xml-nodes-to-map))


(defn source-data
  "Takes xml data as parsed by xml/parse,
   returns a seq of maps of the sources and their data and listeners"
  [zxml]
  (for [z-source (zx/xml-> zxml :source)]
    {:mount (zx/xml1-> z-source (zx/attr :mount))
     :count (Long/parseLong (zx/xml1-> z-source :Listeners zx/text))
     :listeners (-> z-source
                    (zx/xml-> :listener zip/node listeners))}))




(defn mount-count
  "Takes zipped XML tree, returns seq of maps of :mount path and :count listeners"
  [zipped-xml]
  (for [s (zx/xml-> zipped-xml :source)] 
    {:mount (zx/xml1-> s (zx/attr :mount))
     :count (-> s
                (zx/xml1->  :listeners zx/text)
                Long/parseLong)}))

(defn active-mounts
  "Takes seq of maps of :mount and :count, returns only the mounts for counts non-zero"
  [ms]
  (->> ms
       (filter #(-> % :count pos?))
       (map :mount)))


(defn parse-mounts
  [xml]
  (some->> xml
           parse-xml
           mount-count
           active-mounts))

(defn get-mounts
  [{:keys [host port adminuser adminpass]}]
  (try
    (some->> (client/get (format "http://%s:%d/admin/stats.xml" host port)
                         {:basic-auth [adminuser adminpass]
                          ;;:retry-handler utils/retry XXX broken, use a non-home-grown library
                          })
             :body)
    (catch Exception e
      (log/error e))))


(defn eliminate-headers
  "Wipes out the initial <?xml>from a string of xml"
  [s]
  (->> (.split s "\n")
       ;; TODO: stupid, just use a regexp
       (remove #(boolean (re-find #"^<\?xml.+>$" %)))))


(defn parse-clients
  [res]
  (some->> res
           eliminate-headers
           first
           parse-xml
           source-data))

(defn get-clients
  "Obtain the detailed stats as XML for the mountpoint supplied.
   Remove the XML headers for later concatenation and passing along to server."
  [mount {:keys [host port adminuser adminpass]}]
  (try (some->> (client/get (format "http://%s:%d/admin/listclients" host port)
                            {:basic-auth [adminuser adminpass]
                             ;;:throw-exceptions false
                             :query-params {:mount mount}})
                :body)
       (catch Exception e
         (log/error e))))

(defn combine-stats
  [client-maps]
  (some->> client-maps
           (mapcat identity) ;; XXX what???
           (utilza/mapify :mount)))

(defn get-combined-stats
  "Takes seq of strings of mounts. Returns all the stats for the mounts as an xml string"
  ([mounts settings]
   (some->> mounts
            (map #(-> %
                      (get-clients  settings)
                      parse-clients))
            combine-stats))
  ([settings]
   (some-> settings
           get-mounts
           parse-mounts
           (get-combined-stats settings))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  
  )
