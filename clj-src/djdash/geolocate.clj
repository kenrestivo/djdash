(ns djdash.geolocate
  (:require [clj-http.client :as client]
            [clojure.core.async :as async]
            [com.stuartsierra.component :as component]
            [honeysql.helpers :as h]
            [djdash.stats :as stats]
            [honeysql.core :as sql]
            [utilza.log :as ulog]
            [clojure.java.jdbc :as jdbc]
            [honeysql.core :as sql]
            [djdash.utils :as utils]
            [taoensso.timbre :as log]
            [utilza.core :as utilza]
            [utilza.misc :as umisc]))

(def geo-keymap {:cityName :city
                 :countryName :country
                 :longitude :lng
                 :ipAddress :ip
                 :latitude  :lat
                 :regionName :region})

(def convert-types
  (partial umisc/munge-columns {:cityName umisc/capitalize-words
                                :regionName umisc/capitalize-words
                                :latitude #(Double/parseDouble %)
                                :longitude #(Double/parseDouble %)
                                :countryName umisc/capitalize-words}))



(defn db->geo
  "Takes a db connection and an ip address.
  Looks up the IP addr in the db and returns the record for it as a map"
  [conn ip]
  (try
    (->> {:select [:*]
          :from [:geocode]
          :where [:= :ip ip]}
         sql/format
         (jdbc/query conn)
         first)
    (catch Exception e
      (log/error (.getCause e)))))


(defn geo->db!
  "Takes a db connection and a geo record. Inserts it into the db."
  [conn geo]
  (try
    (jdbc/execute! conn
                   (-> (h/insert-into :geocode)
                       (h/values  [geo])
                       sql/format))
    (catch Exception e
      (log/error e (.getCause e)))))


(defn munge-geo
  "Takes a geo record as returned from the geolocation web service.
  Returns it with types and keynames converted and constrained."
  [m]
  (-> m
      convert-types ;; damn well better do this before changing the key names!
      (utilza/select-and-rename geo-keymap)))




(defn fetch-geo
  "Takes an ip address, the URL of the geolocation service, the api key, and retry wait (in milliseconds) and max retries count params.
   Calls the web service and returns the geolocation record."
  [ip url api-key retry-wait max-retries]
  (log/debug "fetching geo from api:" ip)
  (try
    (->> (client/get url {:query-params {:ip ip,
                                         :key api-key
                                         :format "json"}
                          :headers {"Accept" "application/json"}
                          :retry-handler (utils/make-retry-fn retry-wait max-retries false)
                          :as :json})
         :body)
    (catch Exception e
      (log/warn ip "failed to fetch from geoip provider")
      {:ip ip
       :city "Unknown"
       :country "Unknown"
       :lat 0.0 
       :lng 0.0
       :region "Unknown"})))

(defn merge-and-keyify-geo
  "Takes a single listeners data deets record that has come from icecast and been formatted,
  and a map of the geo data for this listener.
  Returns a map with the id as key and the listener data with geo data merged in,
  and id and user agent removed"
  [{:keys [id] :as conn-item-map} geo-data]
  {id (-> conn-item-map
          (merge geo-data)
          (dissoc :user-agent) ;;; XXX hack, just in case it's the problem
          (dissoc :connected)
          (dissoc :id))})


(defn broadcast-new-geos!
  "Takes a sente something and the new connection info map.
  Sends the new map out to all the connected users via sente."
  [sente conns]
  (log/trace "sending out geos to the world" conns)
  (try
    (utils/broadcast sente :djdash/new-geos conns)
    (catch Exception e
      (log/error e))))



(defn fetch-or-save-geo!
  "Takes a settings map, dbc and an ip address
  Looks up the IP, if it's in the db, returns it.
  If not, looks it up from geo service, formats, it, saves to db, and returns it"
  [{:keys [ratelimit-delay-ms url api-key retry-wait max-retries]} dbc ip]
  (let [found-geo (db->geo dbc ip)]
    (if (map? found-geo) ;; it's there
      found-geo
      (let [fetched-geo (->> (fetch-geo ip url api-key retry-wait max-retries)
                             munge-geo)]
        (geo->db! dbc fetched-geo)
        fetched-geo))))

(defn update-geos!
  "Takes a connections atom, a seq of formatted constrained detail maps of the listeners (containing :ip, :user-agent, and :id),
  a sente connection, and a DB connection to lookup/update.
  Returns if all the IDs in deets are identical to the ones in the connections atom (no change).
  Otherwise, for each that is in deets but not in conn, looks each one up in the geo DB.
  If it isn't in there, calls the geo service,
  and adds the geo to db, then adds it to the atom.
  Removes any conns from the atom that are no longer in deets.
  Broadcasts the new atom value to sente."
  [settings conns deets sente dbc]
  (let [deets-ids (->> deets (map :id) set)
        conns-ids (-> @conns keys set)
        new-conn-ids (set (apply disj deets-ids  conns-ids))
        dead-conn-ids (set (apply disj conns-ids  deets-ids))]
    (when-not (= deets-ids conns-ids) ;; no change, do nothing
      (doseq [deet (filter #(-> % :id new-conn-ids) deets)
              :let [{:keys [ip]} deet
                    geo (fetch-or-save-geo! settings dbc ip)]]
        (swap! conns merge (merge-and-keyify-geo deet geo)))
      (swap! conns #(apply (partial dissoc %) dead-conn-ids)) ;; XXX is this right?
      (broadcast-new-geos! sente @conns))))

(defn process-geos
  "Takes geo settings, connections atom, sente channel, db connection, and incoming request channel.
  Processes the geos and updates the db, sente, and atom."
  [settings conns sente dbc combined]
  (log/trace "got request" combined)
  (try
    (when-let [deets (stats/listener-details combined)]
      (log/trace "updating geos" deets)
      (update-geos! settings conns deets sente dbc))
    (catch Exception e
      (log/error e combined))))

(defn start-request-loop
  "Starts a channel/loop that accepts combined stats from nowplaying.
   When it receives one, sends it off to update-geos to fetch the current geo from db or API."
  [{:keys [dbc conns sente settings] :as this}]
  {:pre [ (every? (comp not nil?) [dbc  sente conns])]}
  (let [request-ch (async/chan (async/sliding-buffer 5000))]
    (future (try
              (log/info "starting request loop")
              (loop []
                (let [combined (async/<!! request-ch)]
                  (when-not (= (some-> combined :cmd) :quit) ;; handle in band shutdown
                    (process-geos settings conns sente dbc combined)
                    (recur))))
              (catch Exception e
                (log/error e)))
            (log/info "exiting request loop"))
    (log/info "request loop started")
    (-> this
        (assoc  :request-ch request-ch))))




(defn start-sente-loop
  "Takes a Geo component record.
   Starts a channel/loop that waits for welcome messages from clients,
   and sends them the state of the agent (all connections currently active).
   Assocs a :quit-ch, into the Geo record, when that channel gets a message, shuts dow the loop"
  [{:keys [conns sente] :as this}]
  {:pre [(= clojure.lang.Atom (type conns))
         (every? (comp not nil?) [sente conns])]}
  (let [{:keys [chsk-send! recv-pub]} sente
        sente-ch (async/chan (async/sliding-buffer 1000))
        quit-ch (async/chan)]
    (async/sub recv-pub  :djdash/geo sente-ch)
    (future (try
              (log/info "Starting sente sub for geo channel")
              (loop []
                (let [[{:keys [id client-id ?data]} ch] (async/alts!! [quit-ch sente-ch])]
                  (when (= ch sente-ch)
                    (log/debug "sub sending reply to" client-id ?data)
                    (chsk-send! client-id [:djdash/new-geos @conns])
                    (recur))))
              (catch Exception e
                (log/error e)))
            (log/info "exiting sub for geo")
            ;; TODO: should probably close the channels i created too here
            (async/unsub recv-pub :djdash/geo sente-ch))
    (log/info "sente loop started")
    (assoc this :quit-ch quit-ch)))




;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn start-geo
  [{:keys [dbc settings sente] :as this}]
  {:pre [ (every? (comp not nil?) [dbc settings sente])]}
  (let [conns (atom  {})]
    (log/info "start-geo")
    (-> this
        (assoc :conns conns)
        start-request-loop
        start-sente-loop)))


(defn stop-geo
  [{:keys [conns request-ch] :as this}]
  (log/info "stopping geo")
  (async/>!! request-ch {:cmd :quit})
  (-> this
      (assoc  :sente nil)
      (assoc  :request-ch nil)
      (assoc  :dbc nil)))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defrecord Geo [settings conns sente request-ch dbc]
  component/Lifecycle
  (start
    [this]
    (log/info "starting geo " (-> this :settings (umisc/redact :api-key)))
    (if (and conns request-ch dbc)
      this 
      (try
        (-> this
            (assoc :sente (-> this :sente :sente))
            (assoc :dbc (-> this :db :conn)) ;; need local ref
            start-geo )
        (catch Exception e
          (log/error e)
          (log/error (.getCause e))
          this))))
  (stop
    [this]
    (log/info "stopping geo " (-> this :settings (umisc/redact :api-key)))
    (if-not  (or conns request-ch dbc)
      this
      (do
        (log/debug "branch hit, stopping" this)
        (try 
          (stop-geo this)
          (catch Exception e
            (log/error e)
            (log/error (.getCause e))
            this))))))



(defn create-geo
  [settings]
  (log/info "geo " (umisc/redact settings :api-key))
  ;; TODO: verify all the settings are there and correct
  (component/using
   (map->Geo {:settings settings})
   [:log :db :sente]))





;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment



  (do
    (require '[djdash.core :as sys])
    (require '[utilza.repl :as urepl])
    )


  (ulog/catcher
   (swap! sys/system component/stop-system [:geo]))
  
  (log/set-level! :trace)

  (do
    (swap! sys/system component/stop-system [:geo])
    (swap! sys/system component/start-system [:geo])
    )
  
  (->> @sys/system :geo  :conns deref vals (map :ip))

  (->> @sys/system :geo  :conns deref)
  
  (->> @sys/system :db :conn)

  (-> @sys/system :db :conn (db->geo "foo"))
  
  (->> @sys/system :geo  vals (map type))

  (->> @sys/system :geo  :request-ch async/poll!)
  
  ;; there's crap in there! so the problem is the channel is not getting polled. why?
  (->> #(->> @sys/system :geo  :request-ch async/poll!)
       repeatedly
       (take-while identity)
       ;;count
       #_(urepl/massive-spew "/tmp/foo.edn")
       )
  

  
  (async/>!! (->> @sys/system :db :cmd-ch) {:cmd :save})


  ;; STFU
  (log/merge-config! {:ns-blacklist ["djdash.matrix" "djdash.geolocate"]})


  ;; works manually, so it's async that's being a problem
  (process-geos (-> @sys/system :geo :settings)
                (-> @sys/system :geo :conns)
                (-> @sys/system :geo :sente)
                (-> @sys/system :geo :dbc)
                (-> @sys/system :nowplaying :settings stats/get-combined-stats))
  


  ;; TODO: stick a watch function on conns, see who or what is nuking what?

  (log/merge-config! {:ns-whitelist ["djdash.geolocate"]})
  
  )
