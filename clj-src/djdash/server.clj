(ns djdash.server
  (:require [clojure.core.async :as async]
            [com.stuartsierra.component :as component]
            [djdash.web :as web]
            [djdash.utils :as utils]
            [org.httpkit.server :as kit]
            [taoensso.sente :as sente]
            [taoensso.sente.server-adapters.http-kit :as skit]
            [taoensso.timbre :as log]))



(defrecord Server [db
                   geo
                   nowplaying
                   scheduler
                   sente
                   settings
                   srv]
  component/Lifecycle
  (start
    [this]
    (if srv
      this
      (try
        (log/info "starting webserver " (:settings this))
        ;; TODO: there are many more params to httpsrv/run-server fyi. expose some?
        (let [conns (-> this :geo :conns)
              dbc (-> this :db :conn)
              nowplaying (-> this :nowplaying :nowplaying-internal :nowplaying)
              schedule-agent (-> this :scheduler :scheduler-internal :schedule)
              sente (-> this :sente :sente)
              server (-> (web/make-handler)  
                         (utils/wrap :conns conns)
                         (utils/wrap :dbc dbc)
                         (utils/wrap :nowplaying nowplaying)
                         (utils/wrap :schedule-agent schedule-agent)
                         (utils/wrap :sente sente)
                         (utils/wrap :settings settings)
                         (kit/run-server  {:ip (-> this :settings :address)
                                           :port (-> this :settings :port)}))]
          ;; Finally, return the stateful DI-infested beast
          (assoc this :srv server))
        (catch Exception e
          (log/error e "<- explosion in webserver start")
          (log/error (.getCause e) "<- was cause of explosion" )))))
  (stop
    [this]
    (log/info "stopping webserver " (:settings this))
    (if-not (:srv this)
      this
      (do
        (log/info "server running, stopping it..")
        (web/reload-templates)
        ;; TODO: shut down sente channels
        ((:srv this))
        ;; (srv) shuts it down, be sure to return the component either way!
        (assoc this  
               :srv nil)))))




(defn start-server
  [settings]
  (log/info "server " settings)
  (try
    (component/using
     (map->Server {:settings settings})
     ;; pretty sure order matters here
     [:log :db :sente :scheduler :nowplaying :geo]) 
    (catch Exception e
      (println e))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment
  
  (require '[djdash.core :as sys])


  (do
    (swap! sys/system component/stop-system [:web-server])
    (swap! sys/system component/start-system [:web-server])
    )  

  (def s (setup-sente))


  (def srv (kit/run-server h {:port 8080}))
  
  (srv)

  (->> @sys/system :web-server :schedule-agent deref :future)


  (-> @sys/system :web-server  :scheduler :scheduler-internal :schedule)

  )
