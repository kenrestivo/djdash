#!/bin/bash

sudo -u postgres createuser djdash
sudo -u postgres createdb  -O djdash djdash
sudo -u postgres psql -c "ALTER USER djdash PASSWORD '${PASSWORD}';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE djdash to djdash;"
