(ns user
  (:require [clojure.test :refer :all]
            [utilza.file :as file]
            [utilza.log :as ulog]
            [utilza.repl :as urepl]
            [djdash.core :as dash]
            [clj-http.client :as client]
            [figwheel-sidecar.repl-api :as ra]
            [djdash.stats :as stats]
            [djdash.schedule :as sched]
            [cheshire.core :as json]
            [clojure.edn :as edn]
            [taoensso.timbre :as log]
            [djdash.nowplaying.parse :as p]
            [schema.core :as s]))




(defn start [] (ra/start-figwheel!))

(defn stop [] (ra/stop-figwheel!))

(defn cljs [] (ra/cljs-repl "dev"))



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment


  ;; XXX note live!!
  (ulog/catcher
   (let [p (->> "/mnt/sdcard/tmp/whynot.json"
                slurp
                json/decode
                (rand-nth))]
     (log/trace p)
     (client/post "http://radio.spaz.org:8080/now-playing"
                  {:form-params p
                   :content-type :json
                   :throw-exceptions true})))

  (ulog/spewer
   (client/post "http://localhost:8080/now-playing"
                {:form-params {:foo "bar"}
                 :content-type :json
                 :as :json}))

  
  )



;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  (ulog/catcher 
   (dash/stop)
   (stop)
   (future (dash/-main "config.edn"))
   (start)
   )
  


  (log/set-level! :info)
  

  

  )


(comment

  
  (ulog/spewer
   (let [settings (->> "/home/cust/spaz/src/dash-configs/wtf.edn"
                       slurp
                       edn/read-string
                       :now-playing)]
     (-> settings
         stats/get-combined-stats)))


  

  (ulog/spewer
   (sched/fetch-schedule "http://radio.spaz.org/api/week-info/"))
  
  
  )
