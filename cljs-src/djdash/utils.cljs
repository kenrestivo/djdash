(ns djdash.utils
  (:require 
   [cljs-time.format :as time]
   [cljs-time.coerce :as coerce]
   [cljs-time.core :as tcore]
   [goog.string :as gstring]
   [clojure.walk :as walk]
   [clojure.string :as s])
  (:import [goog.net Jsonp]
           [goog Uri]))


;; this is bad


(defn un-json
  [res]
  (js->clj res :keywordize-keys true))

;; TODO: !! this is a !
(defn jsonp-wrap
  [uri f]
  (-> uri
      Uri.
      Jsonp.
      (.send  nil f)))


;; TODO: unit test
(defn hack-list-group
  [msgs]
  (str "<ul class='list-group'>"
       (->> msgs
            (map #(str "<li class='list-group-item'>" % "</li>\n"))
            (apply str))
       "</ul>"))

;; TODO: unit test
(defn hack-users-list
  [users-string]
  (s/split users-string  #"<br>"))


;; TODO: unit test
(defn reverse-split
  [s]
  (->> s
       (#(str % "<br>")) ;; hack, last message doesn't have a br, due to interpose on server
       (#(.split % "\n"))
       js->clj
       (remove (partial = "<br>"))
       (remove (partial = ""))
       reverse))

;; TODO: unit test
(defn buffer-tick
  [n axis]
  (str (-> n int (/ 1000)) "k"))

;; TODO: Unit test
(defn min-chat-stamp
  ([d]
   (-> d
       .getTime
       (/ 1000)
       (- (* 60 60 24 30))
       Math/floor))
  ([]
   (min-chat-stamp (js/Date.))))




;; TODO: unit test
(defn format-time
  [d]
  (cljs-time.format/unparse
   (cljs-time.format/formatter "h:mma")
   (goog.date.DateTime.  d)))



;; TODO: unit test
(defn short-weekday
  [d]
  (.toLocaleString d js/window.navigator.language #js {"weekday" "short"}))

;; remove this, just check the value in the json
(defn live?
  [playing-text]
  (->> playing-text
       (re-find  #"^\[LIVE\!\].*?")
       boolean))

;; TODO: unit tests
(defn format-schedule-item
  [name start_timestamp end_timestamp]
  (gstring/format "%s %s - %s:   %s"
                  (short-weekday start_timestamp) 
                  (format-time start_timestamp)
                  (format-time end_timestamp)
                  ;; New libretime escapes these, so they show up already escaped.
                  (gstring/unescapeEntities name)))

;; TODO: unit test, hey, isn't this in utilza now?
(defn changed-keys
  "Returns a set of keys in map bm that are not present in map am"
  [am bm]
  (apply disj (-> bm keys set) (keys am)))
