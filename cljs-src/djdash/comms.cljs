(ns djdash.comms
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require  [taoensso.timbre :as log
              :refer-macros (log  trace  debug  info  warn  error  fatal  report
                                  logf tracef debugf infof warnf errorf fatalf reportf
                                  spy get-env log-env)]
             [taoensso.sente  :as sente :refer (cb-success?)]
             [goog.style :as gstyle]
             [reagent.core :as reagent :refer [atom]]
             [reagent.session :as session]
             [cljs-http.client :as http]
             [cljs-time.core :as time]
             [djdash.utils :as utils]
             [djdash.state :as state]
             [goog.string :as gstring]
             [cljs-time.coerce :as coerce]
             [cljs-time.format :as tformat]
             [clojure.walk :as walk]
             [cljs.core.async :as async :refer [put! chan <!]])
  (:import [goog.net Jsonp]
           [goog Uri]))




(defn update-listeners
  [state]
  ;; TODO: only update if it's not checking...!
  (update-in state [:playing :data 0] conj [(js/Date.now) (-> state :playing :listeners)]))




;; TODO: unit test
;;; TODO: Move with below?
(defn get-currently-scheduled
  [{:keys [name start_timestamp end_timestamp]}]
  (let [now  (cljs-time.core/now)]
    (if (and start_timestamp end_timestamp
             (cljs-time.core/after?   now start_timestamp)
             (cljs-time.core/before?  now  end_timestamp))
      (utils/format-schedule-item name start_timestamp end_timestamp)
      "Nobody (Random Archives)")))

;; TODO: unit test
;;; TODO: move?
(defn update-scheduled-now
  [old-app-state]
  (assoc-in old-app-state [:schedule :now]
            (-> old-app-state :schedule :data :current last get-currently-scheduled)))





;; TODO: unit test
(defn update-buffer
  [state {date :date
          min-val :min}]
  (update-in state [:buffer :data 0] conj
             [date (min min-val
                        (-> state :buffer :chart-options :yaxis :max))]))


;; todo: unit test
;; TODO: move this to :component-did-update of geo
(defn update-geos
  [{:keys [connections geo-map markers] :as old-state} msg]
  (if (=  connections msg)
    old-state ;; nothing to see here, move along, move along
    ;; TODO: use changed-keys here
    (let [new-connections (into {} (select-keys msg (apply disj (-> msg keys set)
                                                           (keys connections))))
          new-markers {}
          dead-marker-keys (utils/changed-keys msg connections)]
      (debug "map changed" new-connections new-markers dead-marker-keys)
      (let [debug-new-markers (merge (apply dissoc markers dead-marker-keys)
                                     new-markers)]
        (debug debug-new-markers)
        (-> old-state
            (assoc :markers debug-new-markers)
            (assoc :connections msg))))))


;; TODO: unit test, though watch out there's a js/datenow in there
(defn dispatch-message
  [old-state [id msg]]
  (case id
    :djdash/buffer (update-buffer old-state msg)
    :djdash/new-geos (update-in old-state [:geo] update-geos msg)
    :djdash/now-playing (let [{:keys [listeners playing] :as new-data} msg]
                          (-> old-state
                              (update-in [:playing] merge
                                         (select-keys new-data [:listeners :playing]))
                              (assoc-in  [:playing :live?] (utils/live? playing))
                              (update-in  [:playing :data 0] conj [(js/Date.now)
                                                                   listeners])))
    :djdash/next-shows (-> old-state
                           (assoc-in [:schedule :data] msg)
                           update-scheduled-now)
    (do (error "unknown message type" id msg)
        ;; preserve old state, and return it as the fallback
        old-state)))


(defn request-updates
  [{:keys [chsk-send!]}]
  (chsk-send! [:djdash/schedule {:cmd :refresh}])
  (chsk-send! [:djdash/now-playing {:cmd :refresh}])
  (chsk-send! [:djdash/geo {:cmd :refresh}]))



;; TODO: unit test this
(defn set-to-checking
  [old-state]
  (info "disconnected, setting to checking")
  (let [{:keys [checking]} state/strs]
    (-> old-state
        (assoc-in [:playing :playing] checking)
        (assoc-in [:playing :listeners] checking)
        (assoc-in [:schedule :now] checking))))


(defn dispatch-event
  [{:keys [?data id]}]
  ;; TODO: try/catch here to as to not choke the router
  (case id
    :chsk/recv (swap! state/app-state dispatch-message ?data)
    :chsk/state  (let [[o n] ?data]
                   (cond
                     (:first-open? n) (request-updates (:sente @state/app-state))
                     ;; TODO: CHECK taht this works
                     (:disconnected n) (swap! state/app-state set-to-checking)))
    ;; ignore handshakes, etc
    nil))


;; this actually starts the routing going
(defn start-sente
  []
  (debug "starting sente")
  (swap! state/app-state
         (fn [{:keys [settings] :as orig-state}]
           (assoc orig-state :sente
                  (let [{:keys [chsk ch-recv send-fn state]} (sente/make-channel-socket!
                                                              "/djdash/ch"
                                                              {:type :auto })]
                    {:chsk    chsk
                     :ch-chsk    ch-recv
                     :chsk-send! send-fn
                     :chsk-state state
                     :event-router (sente/start-chsk-router! ch-recv dispatch-event)})))))



(defn timestampify
  "Takes a message map.
  If there is no time_received, adds the current datetime milliseconds since utc epoch"
  [{:keys [time_received] :as msg}]
  (if time_received
    msg
    (assoc msg :time_received (or time_received
                                  (-> (js/Date.)
                                      .getTime)))))

(defn start-chat
  []
  ;; guard against double-instantiation in figwheel
  ;;  TODO: XXX but remove in prod. don't want to fail logins?
  (when-not (-> @state/app-state :chat :login)
    (swap! state/app-state update-in [:chat] merge
           (-> js/settings ;; these are the settings passed in from the html. hacky, but necessary
               utils/un-json
               :chat
               (merge {:onMessage (fn [msg]
                                    (swap! state/app-state update-in [:chat :messages]
                                           (fn [o]
                                             (cons (-> msg
                                                       utils/un-json
                                                       timestampify)
                                                   o))))
                       :onRosterChanged #(swap! state/app-state assoc-in [:chat :users] (utils/un-json %))
                       :onConnected (fn [_]
                                      (swap! state/app-state assoc-in [:chat :connected?] true)
                                      ;; Run the login function
                                      ((-> @state/app-state :chat :login)
                                       (-> @state/app-state :chat :user clj->js)))
                       :onDisconnected (fn [_]
                                         (swap! state/app-state
                                                #(-> %
                                                     (assoc-in [:chat :connected?] false)
                                                     (assoc-in [:chat :users] [(:checking state/strs)])
                                                     (assoc-in [:chat :messages] []))))})

               clj->js
               js/spaz_radio_chat.
               utils/un-json))))


(defn add-watches
  []
  (add-watch state/app-state ::username-change
             (fn [_ _ old-val new-val]
               (let [old-user (some-> old-val :chat :user)
                     new-user (some-> new-val :chat :user)
                     login-fn (-> new-val :chat :login)]
                 (when (not= old-user new-user)
                   (.set state/storage :user new-user)
                   (login-fn (clj->js new-user)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  (log/set-level! :trace)


  (-> @state/app-state :chat :user)

  (-> js/settings utils/un-json :chat)

  (-> @state/app-state :chat :connected?)

  )
